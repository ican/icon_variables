import numpy as np

chunk_bases = {
    "time": 1,
    "level_full": 30,
    "level_half": 31,
    "depth_full": 32,
    "depth_half": 33,
}

def make_chunks(dims, shape, target_size, itemsize=4, hints=None):
    target_size = target_size // itemsize
    hints = hints or {}
    shape = [s if s > 0 else np.inf for s in shape]
    if target_size >= np.prod(shape):
        return shape

    sd = dict(zip(dims, shape))
    chunks = {d: min(chunk_bases.get(d, 1), s) for d, s in zip(dims, shape)}
    if "cell" in dims:
        chunks["cell"] = min(4**9, sd["cell"])
        free_dims = [d for d, s in zip(dims, shape) if d != "cell"]
    else:
        free_dims = dims

    next_free_dim = 0
    while np.prod(list(chunks.values())) < target_size and free_dims:
        focus_dim = free_dims[next_free_dim]
        dim_hints = hints.get(focus_dim, {})
        still_free = True

        if steps := hints.get(focus_dim, {}).get("steps", None):
            for s in steps:
                if s > chunks[focus_dim]:
                    chunks[focus_dim] = s
                    break
            else:
                still_free = False
        else:
            chunks[focus_dim] *= 2

        if chunks[focus_dim] >= sd[focus_dim]:
            chunks[focus_dim] = sd[focus_dim]
            still_free = False

        if "max" in dim_hints and chunks[focus_dim] >= dim_hints["max"]:
            chunks[focus_dim] = dim_hints["max"]
            still_free = False

        if still_free:
            next_free_dim += 1
        else:
            free_dims = free_dims[:next_free_dim] + free_dims[next_free_dim + 1:]

        if next_free_dim >= len(free_dims):
            next_free_dim = 0

    return tuple(chunks[d] for d in dims)


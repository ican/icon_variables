import yaml
from .var_utils import normalize_varname

def listify(e):
    if isinstance(e, list):
        return e
    else:
        return [e]

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("config")
    parser.add_argument("category", nargs="+")
    args = parser.parse_args()

    config = yaml.load(open(args.config), Loader=yaml.SafeLoader)

    print(", ".join(sorted(set(v
                               for v, cats in config["variables"].items()
                               if any(c in listify(cats) for c in args.category)),
                           key=normalize_varname)))

if __name__ == "__main__":
    exit(main())

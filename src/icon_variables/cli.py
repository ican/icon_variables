from .expand_plan import configure_expand_plan_parser
from .generate_component_havelists import configure_havelist_parser
from .render import configure_render_parser

def get_parser():
    import argparse
    parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.set_defaults(func=None)
    subparsers = parser.add_subparsers()
    configure_expand_plan_parser(subparsers.add_parser('expand_plan'))
    configure_havelist_parser(subparsers.add_parser('havelist'))
    configure_render_parser(subparsers.add_parser('render'))
    return parser

def main():
    import sys
    args = get_parser().parse_args()

    if not args.func:
        print("please provide a subcommand, run with -h for help", file=sys.stderr)
        return -1

    return(args.func(args))


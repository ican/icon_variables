#!/bin/bash

mkdir -p public
configurations=`find configurations/*.yaml | cut -d / -f 2 | cut -d . -f 1`
for c in $configurations; do
    echo "generating $c"
    icon_variables render "configurations/$c.yaml" "public/$c.html"
    icon_variables expand_plan "configurations/$c.yaml" > "public/$c.plan.yaml"
done

icon_variables havelist > public/havelist_template.yaml.tmpl
icon_variables havelist --parsable_yaml > public/havelist_template.yaml

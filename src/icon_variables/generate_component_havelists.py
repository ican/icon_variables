import yaml
from pathlib import Path
from collections import defaultdict

from .var_utils import normalize_varname

vgrid_sizes = {
    "reference": "%{atmo.levels}",
    "reference_half": "%{atmo.halflevels}",
    "depth_below_sea": "%{ocean.levels}",
    "depth_below_sea_half": "%{ocean.halflevels}",
    "surface": 1,
    "meansea": 1,
    "height_2m": 1,
    "height_10m": 1,
    "atmosphere": 1,
    "toa": 1,
    "generic_ice": 1,
    "soil_depth_water": "%{soil_depth_water_levels}",
    "soil_depth_energy": "%{soil_depth_energy_levels}",
}

default_component_renames = {
    "jsbach": "atmo",
}

def format_haveitem(name, options):
    options = {k: v for k, v in options.items() if not (k == "size" and v == 1)}
    if len(options) == 0:
        return name
    else:
        return {name: options}

def run(args):
    component_renames = dict(r.split("=", 1) for r in args.rename_component or [])
    for k, v in default_component_renames.items():
        component_renames[k] = component_renames.get(v, v)

    with open(args.varlist) as varfile:
        vardata = yaml.load(varfile, Loader=yaml.SafeLoader)

    vardata = {**vardata, "variables": {normalize_varname(k): v for k, v in vardata.get("variables", {}).items()}}

    vars_by_component = defaultdict(dict)
    for varname, varinfo in vardata["variables"].items():
        if varinfo.get("hgrid", None) != "cell":
            # TODO: support other horizontal source grids
            continue
        if "component" in varinfo and varinfo.get("vgrid", None) is not None:
            component = component_renames.get(varinfo["component"], varinfo["component"])
            vars_by_component[component][varname] = varinfo

    coupling = {"components": {
        componentname: {
            "have": [format_haveitem(varname, {"size": vgrid_sizes[varinfo["vgrid"]]})
                     for varname, varinfo in variables.items()]
        }
        for componentname, variables in vars_by_component.items()
    }}
    coupling_yaml = yaml.dump(coupling)
    if not args.parsable_yaml:
        for v in vgrid_sizes.values():
            if not isinstance(v, str):
                continue
            coupling_yaml = coupling_yaml.replace(f"'{v}'", v)
    print(coupling_yaml)


def configure_havelist_parser(parser):
    parser.add_argument("--parsable_yaml", action="store_true", default=False, help="generate parsable yaml instead of template")
    parser.add_argument("--varlist", type=Path,
        default=Path(__file__).parent.parent.parent / "variables.yaml")
    parser.add_argument("--rename_component", type=str, nargs="*", metavar="atmo=atmo_output", help="rename component names")
    parser.set_defaults(func=run)
    return parser

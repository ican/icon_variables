import re
import math
import numpy as np
from pathlib import Path
from itertools import product
from collections import defaultdict
import yaml
import isodate

from .chunk_utils import make_chunks
from .var_utils import normalize_varname

dims_3d = {
    "reference": "level_full",
    "reference_half": "level_half",
    "depth_below_sea": "depth_full",
    "depth_below_sea_half": "depth_half",
    "soil_depth_water": "soil_depth_water_level",
    "soil_depth_energy": "soil_depth_energy_level",
}

var_attrs = [
    "standard_name",
    "long_name",
    "units",
    "component",
    "vgrid",
]

time_agg_cell_method = {
    "last": "point",
    "mean": "mean",
    "sum": "sum",
}

def listify(e):
    if isinstance(e, list):
        return e
    else:
        return [e]

def expand_member_definition(avail, definition):
    active = set()
    for d in listify(definition):
        if d.startswith("-"):
            negate = True
            d = d[1:]
        else:
            negate = False
        pat = re.compile("^" + d.replace("*", ".*") + "$")
        for member in avail:
            if pat.match(",".join(map(str, member))):
                if negate:
                    active.remove(member)
                else:
                    active.add(member)
    return active


def estimate_memory(variables):
    itemsize = 4  # TODO: this is only valid for <f4 dtype!
    return int(np.sum([np.prod([
        c if d in ["time", "cell"] else s
        for d, s, c in zip(v["dims"], v["shape"], v["chunks"])
        ]) for v in variables.values()])) * itemsize

def split_variables_to_size(varnames, variables, target_size):
    vars_with_size = list(sorted([(estimate_memory({name: variables[name]}), name) for name in variables]))
    total_size = sum(s for s, _ in vars_with_size)
    n = max(1, math.ceil(total_size / target_size))
    outlists = [[] for _ in range(n)]
    iout = 0
    for size, var in vars_with_size:
        outlists[iout].append(var)
        iout += 1
        if iout >= len(outlists):
            iout = 0
    return outlists

def group_vars_by_chunking_and_substream(variables, target_size):
    res = defaultdict(set)
    for varname, var in variables.items():
        if "cell" in var["dims"]:
            celldim = var["dims"].index("cell")
            key = var["shape"][celldim] // var["chunks"][celldim]
        else:
            key = 1
        res[key].add(varname)

    return {(k, i): list(sorted(v))
            for k, vs in res.items()
            for i, v in enumerate(split_variables_to_size(vs, variables, target_size))}

def format_group(group):
    return "_".join(map(str, group))

def format_component(group, hchunk, substream=0):
    return f"healpix_output_{group[0]}_h{hchunk}_{substream}"

def check_collect_variable_chunks(variables):
    chunks = {}
    for var in variables.values():
        key = tuple(sorted(var["dims"]))
        varchunks = dict(zip(var["dims"], var["chunks"]))
        sorted_chunks = tuple(varchunks[k] for k in key)
        if key in chunks:
            assert chunks[key] == sorted_chunks
        else:
            chunks[key] = sorted_chunks
    return [dict(zip(k, v)) for k, v in chunks.items()]

def run(args):

    config = yaml.safe_load(open(args.config))
    config = {**config, "variables": {normalize_varname(k): v for k, v in config.get("variables", {}).items()}}

    res_by_dim = {c["dim"]: c["values"] for c in config["resolutions"]}

    if args.variables:
        variables_path = args.variables
    else:
        variables_path = Path(__file__).parent.parent.parent / "variables.yaml"

    with open(variables_path) as varfile:
        vardata = yaml.load(varfile, Loader=yaml.SafeLoader)

    vardata = {**vardata, "variables": {normalize_varname(k): v for k, v in vardata.get("variables", {}).items()}}

    output_groups = set(product(*[d["values"] for d in config["resolutions"]]))

    output_categories = {}
    for category, settings in config["output_categories"].items():
        if "active" in settings:
            is_member_of = expand_member_definition(output_groups, settings["active"])
            output_categories[category] = {**settings, "active": is_member_of, "active_direct": is_member_of}
        else:
            output_categories[category] = settings

    variables = {k: listify(v) for k, v in sorted(config["variables"].items(), key=lambda x: normalize_varname(x[0]))}

    def var_in_group(var, group):
        for c in variables[var]:
            if group in output_categories[c].get("active", []):
                return True
        return False

    def var_in_timeres(var, timeres):
        for c in variables[var]:
            if timeres in set(g[0] for g in output_categories[c].get("active", [])):
                return True
        return False


    def var_time_interp(var, timeres):
        tri = res_by_dim["time"].index(timeres)
        if tri > 0:
            if var_in_timeres(var, res_by_dim["time"][tri - 1]):
                return "mean"

        candidates = []
        for c in variables[var]:
            catinfo = output_categories[c]
            agg = catinfo.get("agg", {})
            if "time" in agg:
                candidates.append(time_agg_cell_method[agg["time"]])

        if not candidates:
            return None
        assert all(c == candidates[0] for c in candidates)
        return candidates[0]


    def format_cell_methods(var, group):
        candidates = []
        for c in variables[var]:
            methods = {}
            catinfo = output_categories[c]
            if group in catinfo.get("active", []):
                if time_interp := var_time_interp(var, group[0]):
                    methods["time"] = time_interp
                if (group[0], group[1] + 1) in catinfo.get("active", []):
                    methods["cell"] = "mean"
            if methods:
                candidates.append(" ".join(f"{k}: {v}" for k, v in methods.items()))

        assert all(c == candidates[0] for c in candidates)
        return candidates[0]


    def dims_of(var):
        vgrid = vardata["variables"][var]["vgrid"]
        if vgrid in dims_3d:
            return ["time", dims_3d[vgrid], "cell"]
        else:
            return ["time", "cell"]

    def group_dimensions(group):
        return {
            **config.get("shape", {}),
            "time": 0,
            "cell": 12 * 4**group[1],
        }

    def var_geometry(var, group):
        dims = dims_of(var)
        dimensions = group_dimensions(group)
        shape = [dimensions[d] for d in dims]
        time_element_period = isodate.parse_duration(group[0])
        chunk_target_size = config.get("chunk_target_size", 10e6)

        def normalize_hint(hint):
            hint = {**hint}
            if len(shape) == 3:
                hint = {**hint, **hint.get("3D", {})}
            if "max_period" in hint:
                hint["max"] = min(hint.get("max", chunk_target_size), int(isodate.parse_duration(hint["max_period"]) / time_element_period))
                del hint["max_period"]
            return hint

        chunk_hints = {k: normalize_hint(v) for k, v in config.get("chunks", {}).items()}
        chunks = make_chunks(dims, shape, chunk_target_size, itemsize=4, hints=chunk_hints)
        nchunks = {d: math.ceil(s / c) for d, s, c in zip(dims, shape, list(chunks))}
        return {
            "dims": dims,
            "shape": shape,
            "chunks": chunks,
            "nchunks": nchunks,
        }

    variables_by_group = {
        group: {
            var: {
                **var_geometry(var, group),
                "group": group,
                "attrs": {**
                    {
                        k: v for k, v in vardata["variables"][var].items()
                        if k in var_attrs
                    },
                    "cell_methods": format_cell_methods(var, group),
                    "grid_mapping": "crs",
                }
            }
            for var in variables
            if var_in_group(var, group)
        }
        for group in output_groups
    }

    #print(yaml.dump(variables_by_group))
    #print()

    streams_by_timeres_and_var = {
        (timeres, var): format_component((timeres, res_by_dim["zoom"][0]), hchunk, substream)
        for timeres in res_by_dim["time"]
        for (hchunk, substream), process_vars in group_vars_by_chunking_and_substream(variables_by_group[timeres, res_by_dim["zoom"][0]], target_size=config.get("worker_target_size", np.inf)).items()
        for var in process_vars
    }

    timeres_of_stream = {v: k[0] for k, v in streams_by_timeres_and_var.items()}
    vars_of_stream = defaultdict(set)
    for (timeres, var), stream in streams_by_timeres_and_var.items():
        vars_of_stream[stream].add(var)

    def group_vars_by_stream(variables):
        res = defaultdict(set)
        for varname, var in variables.items():
            stream = streams_by_timeres_and_var[var["group"][0], varname]
            res[stream].add(varname)
        return {k: list(sorted(v)) for k, v in res.items()}

    def check_get_nchunks(variables):
        nchunks = [v["nchunks"]["cell"] for v in variables.values()]
        assert all(c == nchunks[0] for c in nchunks)
        return nchunks[0]

    couplings = {
        "components": {
            stream: {
                "model": "output",
                "grid": f"healpix_{timeres}",
                "time": {
                    "step": timeres,
                    "lag": 1,
                },
                "want": [
                    {var: {"interpolation": f"output_{var_time_interp(var, timeres)}"}}
                    for var in vars_of_stream[stream]],
            }
            for stream, timeres in timeres_of_stream.items()
        },
        "interpolations": {
            "output_mean": {
                "space":  [{
                    "method": "n-nearest_neighbor",
                    "n": 1,
                    "weighted": "ARITHMETIC_AVERAGE",
                }],
                "time": "average",
            },
            "output_point": {
                "space":  [{
                    "method": "n-nearest_neighbor",
                    "n": 1,
                    "weighted": "ARITHMETIC_AVERAGE",
                }],
                "time": "none",
            },
        }
    }


    process_groups = [
        {
            "group": format_group(group),
            "nchunks": check_get_nchunks({n: variables[n] for n in process_vars}),
            "variables": process_vars,
            "bisections": group[1],
            "hlevel": res_by_dim["zoom"][0] - group[1],
            "hstep": 1 if group[1] != res_by_dim["zoom"][-1] else 0,
            "component": stream,
            "lag": 1,
            "grid": f"healpix_{timeres_of_stream[stream]}",
            "memory_estimate": estimate_memory({n: variables[n] for n in process_vars}),
        }
        for group, variables in variables_by_group.items()
        for stream, process_vars in group_vars_by_stream(variables).items()
    ]

    datasets = {
        format_group(group): {
            "attrs": {},
            "lag": 1,
            "dimensions": {
                **config.get("shape", {}),
                "time": 0,
                "cell": 12 * 4**group[1],
                "crs": 1,
            },
            "chunks": check_collect_variable_chunks(variables),
            "encoding": {
                "compressor": {
                    "blocksize": 0,
                    "clevel": 5,
                    "cname": args.compressor,
                    "id": "blosc",
                    "shuffle": 1,
                },
                "dtype": "<f4",
                "fill_value": "NaN",
                "filters": None,
            },
            "variables": {
                **{
                    var: {
                        k: varinfo[k]
                        for k in ["dims", "attrs"]
                    }
                    for var, varinfo in variables.items()
                },
                "crs": {
                    "dims": ["crs"],
                    "attrs": {
                        "grid_mapping_name": "healpix",
                        "healpix_nside": 2**group[1],
                        "healpix_order": "nest",
                    },
                },
                "time": {
                    "dims": ["time"],
                    "attrs": {
                        "units": "seconds since 1970-01-01",
                        "calendar": "proleptic_gregorian",
                        "axis": "T",
                    },
                    "encoding": {
                        "dtype": "<i4",
                        "fill_value": None,
                    },
                }
            }
        }
        for group, variables in variables_by_group.items()
    }
    plan = {
        "process_groups": process_groups,
        "couplings": couplings,
        "datasets": datasets,
        "multilevel_steps": len(res_by_dim["zoom"]) - 1,
        "config": config,
    }
    print(yaml.dump(plan))


def configure_expand_plan_parser(parser):
    parser.add_argument("config")
    parser.add_argument("--variables", type=Path, metavar="variables.yaml", help="variable list for given model")
    parser.add_argument("--compressor", choices=['lz4', 'zstd'], default='lz4',
        help="name of compression method [%(default)s]")
    parser.set_defaults(func=run)


if __name__ == "__main__":
    exit(main())
